Metafield
=========

Metafield is a simple Lua library for managing metafields.

Docs
----

    local handle = metafield.new(key)

Creates a new handle with the given key.

    metafield.lock(handle)

Locks the key associated with the given handle.

    metafield.unlock(handle)

Unlocks the key associated with the given handle.

    metafield.get(handle, object)

Retrieves the metafield, keyed by the given handle's key, associated with the given object.