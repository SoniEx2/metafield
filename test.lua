local metafield = require "metafield"

do
  local handle1 = metafield.new("__test")
  local handle2 = metafield.new("__test")
  metafield.lock(handle2)
  local ok, err = pcall(metafield.new, "__test")
  assert(not ok and err:find("Metafield '__test' is locked", 1, true))
  metafield.unlock(handle2)
end

local repr
do
  local handle = metafield.new("__repr")
  metafield.lock(handle)
  repr = function(x)
    local field = metafield.get(handle, x)
    if field then
      return field(x)
    else
      return tostring(x)
    end
  end
end

do
  local t = setmetatable({}, {__metatable="no", __repr=function() return "{}" end})
  assert(repr(t) == "{}")
end